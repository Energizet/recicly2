package ru.energizet.pks30.recicly2.activity

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import ru.energizet.pks30.recicly2.GroupModel
import ru.energizet.pks30.recicly2.adapter.GroupsAdapter
import ru.energizet.pks30.recicly2.databinding.ActivityMainBinding
import ru.energizet.pks30.recicly2.state.LoadingState
import ru.energizet.pks30.recicly2.state.State

class MainActivity : AppCompatActivity() {
    private lateinit var L: ActivityMainBinding
    private lateinit var adapter: GroupsAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        L = ActivityMainBinding.inflate(layoutInflater)
        setContentView(L.root)

        val model = ViewModelProvider(this).get(GroupModel::class.java)
            .observe(this, { observe(it) })
            .observeState(this, { stateObserve(it) })
        val groups = model.groups
        adapter = GroupsAdapter(groups)

        L.srlRefresh.setOnRefreshListener { model.fetchTasks() }
        L.rvGroups.addOnScrollListener(onScrollListener)
        L.rvGroups.layoutManager = LinearLayoutManager(this)
        L.rvGroups.adapter = adapter
        L.fab.setOnClickListener { fabClick() }

        val callback = adapter.ItemTouchCallback()
        val swipeHelper = ItemTouchHelper(callback)
        swipeHelper.attachToRecyclerView(L.rvGroups)
    }

    private fun observe(groups: ArrayList<GroupsAdapter.Group>) {
        if (this::adapter.isInitialized) {
            adapter.updateList(groups)
        }
    }

    private fun stateObserve(state: LoadingState) {
        when (state.state) {
            State.RUNNING -> showProgress()
            State.SUCCESS -> hideProgress()
            State.FAILED -> {
                Toast.makeText(this, state.message, Toast.LENGTH_LONG).show()
                hideProgress()
            }
        }
    }

    private fun showProgress() {
        L.progressBar.isVisible = !L.srlRefresh.isRefreshing
        L.rvGroups.isVisible = false
    }

    private fun hideProgress() {
        L.srlRefresh.isRefreshing = false
        L.progressBar.isVisible = false
        L.rvGroups.isVisible = true
    }

    private fun fabClick() {
        val intent = Intent(this, AddTaskActivity::class.java)
        startActivity(intent)
    }

    private val onScrollListener = object : RecyclerView.OnScrollListener() {
        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
            if (dy > 0 && L.fab.isShown) {
                L.fab.hide()
            } else if (dy < 0 && !L.fab.isShown) {
                L.fab.show()
            }
        }
    }
}