package ru.energizet.pks30.recicly2

import android.graphics.Color
import androidx.lifecycle.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import ru.energizet.pks30.recicly2.adapter.GroupsAdapter
import ru.energizet.pks30.recicly2.adapter.NotesAdapter
import ru.energizet.pks30.recicly2.network.Tasks
import ru.energizet.pks30.recicly2.network.Utils
import ru.energizet.pks30.recicly2.state.LoadingState

class GroupModel : ViewModel() {
    private var groupsObject = MutableLiveData<ArrayList<GroupsAdapter.Group>>()
    private val state = MutableLiveData<LoadingState>()

    init {
        fetchTasks()
    }

    fun fetchTasks() {
        GlobalScope.launch(Dispatchers.IO) {
            try {
                state.postValue(LoadingState.LOADING)
                val tasks = Utils.api.getTasks()
                this@GroupModel.groups = parseTasks(tasks)
                state.postValue(LoadingState.LOADED)
            } catch (ex: Exception) {
                state.postValue(LoadingState.error(ex.message))
            }
        }
    }

    private fun parseTasks(tasks: ArrayList<Tasks>): ArrayList<GroupsAdapter.Group> {
        val categories = hashMapOf<String, Int>()
        val groups = arrayListOf<GroupsAdapter.Group>()
        tasks.forEach {
            if (!categories.containsKey(it.category.name)) {
                groups.add(GroupsAdapter.Group(it.category.name, ArrayList()))
                categories[it.category.name] = groups.lastIndex
            }
            groups[categories[it.category.name]!!].notes.add(
                NotesAdapter.Note(
                    it.name,
                    it.description,
                    Color.parseColor(it.priority.color),
                    it.done > 0
                )
            )
        }
        return groups
    }

    var groups: ArrayList<GroupsAdapter.Group>
        get() {
            if (groupsObject.value == null) {
                groupsObject.value = ArrayList()
            }
            return groupsObject.value!!
        }
        set(value) {
            groupsObject.postValue(value)
        }

    fun observe(
        owner: LifecycleOwner,
        observer: Observer<ArrayList<GroupsAdapter.Group>>
    ): GroupModel {
        groupsObject.observe(owner, observer)
        return this
    }

    fun observeState(
        owner: LifecycleOwner,
        observer: Observer<LoadingState>
    ): GroupModel {
        state.observe(owner, observer)
        return this
    }
}