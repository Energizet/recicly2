package ru.energizet.pks30.recicly2.state

enum class State {
    RUNNING,
    SUCCESS,
    FAILED
}