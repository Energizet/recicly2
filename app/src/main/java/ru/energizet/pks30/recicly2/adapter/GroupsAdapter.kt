package ru.energizet.pks30.recicly2.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import ru.energizet.pks30.recicly2.databinding.AdapterNoteGroupBinding

class GroupsAdapter(private var list: ArrayList<Group>) :
    RecyclerView.Adapter<GroupsAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        createSwipeHelper()
        val inflater = LayoutInflater.from(parent.context)
        return ViewHolder(
            AdapterNoteGroupBinding.inflate(inflater, parent, false)
        )
    }

    fun updateList(list: ArrayList<Group>) {
        this.list = list
        createSwipeHelper()
        notifyDataSetChanged()
    }

    private fun createSwipeHelper() {
        list.forEach { group ->
            group.adapter = NotesAdapter(group.notes)
            val callback = group.adapter.ItemTouchCallback()
            group.swipeHelper = ItemTouchHelper(callback)
        }
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.setIsRecyclable(false)
        val group = list[position]
        holder.L.tvGroup.text = group.name
        holder.L.rvNodes.layoutManager = LinearLayoutManager(holder.L.rvNodes.context)
        holder.L.rvNodes.itemAnimator = null
        val adapter = group.adapter
        holder.L.rvNodes.adapter = adapter

        group.swipeHelper.attachToRecyclerView(holder.L.rvNodes)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    class ViewHolder(val L: AdapterNoteGroupBinding) : RecyclerView.ViewHolder(L.root)

    class Group(val name: String, val notes: ArrayList<NotesAdapter.Note>) {
        lateinit var adapter: NotesAdapter
        lateinit var swipeHelper: ItemTouchHelper
    }

    inner class ItemTouchCallback :
        ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.END) {
        override fun onMove(
            recyclerView: RecyclerView,
            viewHolder: RecyclerView.ViewHolder,
            target: RecyclerView.ViewHolder
        ): Boolean {
            return false
        }

        override fun getSwipeDirs(
            recyclerView: RecyclerView,
            viewHolder: RecyclerView.ViewHolder
        ): Int {
            val position = viewHolder.adapterPosition
            return if (list[position].notes.size > 1) 0
            else super.getSwipeDirs(
                recyclerView,
                viewHolder
            )
        }

        override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
            val position = viewHolder.adapterPosition
            list.removeAt(position)
            notifyItemRemoved(position)
        }
    }
}