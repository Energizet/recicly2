package ru.energizet.pks30.recicly2.network

import com.google.gson.annotations.SerializedName

data class Tasks(
    @SerializedName("id")
    val id: Int,
    val category: Category,
    val name: String,
    val description: String,
    val priority: Priority,
    val done: Int
) {

    data class Category(val name: String)
    data class Priority(val color: String)
}