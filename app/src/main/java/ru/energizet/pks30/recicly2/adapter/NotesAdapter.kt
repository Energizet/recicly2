package ru.energizet.pks30.recicly2.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import ru.energizet.pks30.recicly2.databinding.AdapterNoteBinding

class NotesAdapter(private val list: ArrayList<Note>) :
    RecyclerView.Adapter<NotesAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return ViewHolder(
            AdapterNoteBinding.inflate(inflater, parent, false)
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val note = list[position]
        holder.L.tvTitle.text = note.title
        holder.L.tvText.text = note.text
        holder.L.vIndicator.setBackgroundColor(note.indicator)
        holder.L.cbDone.isChecked = note.checked
        holder.L.cbDone.setOnCheckedChangeListener { _, isChecked ->
            note.checked = isChecked
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }

    class ViewHolder(val L: AdapterNoteBinding) : RecyclerView.ViewHolder(L.root)

    class Note(
        val title: String,
        val text: String,
        val indicator: Int,
        var checked: Boolean
    )

    inner class ItemTouchCallback :
        ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.END) {
        override fun onMove(
            recyclerView: RecyclerView,
            viewHolder: RecyclerView.ViewHolder,
            target: RecyclerView.ViewHolder
        ): Boolean {
            return false
        }

        override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
            val position = viewHolder.adapterPosition
            list.removeAt(position)
            notifyItemRemoved(position)
        }
    }
}