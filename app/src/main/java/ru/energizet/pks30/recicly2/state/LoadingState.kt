package ru.energizet.pks30.recicly2.state

import androidx.lifecycle.MutableLiveData

class LoadingState private constructor(
    val state: State,
    val message: String? = null
) {
    companion object {
        val LOADING = LoadingState(State.RUNNING)
        val LOADED = LoadingState(State.SUCCESS)

        fun error(message: String?) = LoadingState(State.FAILED, message)
    }
}