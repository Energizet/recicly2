package ru.energizet.pks30.recicly2.network

import retrofit2.http.GET

interface Api {
    @GET("tasks")
    suspend fun getTasks(): ArrayList<Tasks>
}