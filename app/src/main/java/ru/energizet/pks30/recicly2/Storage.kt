package ru.energizet.pks30.recicly2

import android.graphics.Color
import ru.energizet.pks30.recicly2.adapter.GroupsAdapter
import ru.energizet.pks30.recicly2.adapter.NotesAdapter

@Deprecated("Moved to network")
object Storage {
    enum class Indicator(val value: Int) {
        Red(Color.parseColor("#fc4040")),
        Green(Color.parseColor("#64c16d")),
        Blue(Color.parseColor("#6083fe")),
        Yellow(Color.parseColor("#ffc453")),
    }

    fun createGroups(): ArrayList<GroupsAdapter.Group> {
        return arrayListOf(
            GroupsAdapter.Group("Работа", arrayListOf(
                NotesAdapter.Note("Поглабить котика", "Нежно и акуратно", Indicator.Red.value, true),
                NotesAdapter.Note("Попробовать камчатского краба", "Оно того стоит", Indicator.Blue.value, false),
            )),
            GroupsAdapter.Group("Учёба", arrayListOf(
                NotesAdapter.Note("Сесть за лабу вовремя", "Без регистрации и смс", Indicator.Yellow.value, true),
                NotesAdapter.Note("Промспать первуб пару", "А то экстрима в жизни не хватает", Indicator.Blue.value, false),
                NotesAdapter.Note("Подумать о будещем", "Положить сотку в карман вертровки", Indicator.Green.value, true),
            )),
            GroupsAdapter.Group("Учёба", arrayListOf(
                NotesAdapter.Note("Сесть за лабу вовремя", "Без регистрации и смс", Indicator.Yellow.value, true),
                NotesAdapter.Note("Промспать первуб пару", "А то экстрима в жизни не хватает", Indicator.Blue.value, false),
                NotesAdapter.Note("Подумать о будещем", "Положить сотку в карман вертровки", Indicator.Green.value, true),
            )),
            GroupsAdapter.Group("Учёба", arrayListOf(
                NotesAdapter.Note("Сесть за лабу вовремя", "Без регистрации и смс", Indicator.Yellow.value, true),
                NotesAdapter.Note("Промспать первуб пару", "А то экстрима в жизни не хватает", Indicator.Blue.value, false),
                NotesAdapter.Note("Подумать о будещем", "Положить сотку в карман вертровки", Indicator.Green.value, true),
            )),
            GroupsAdapter.Group("Учёба", arrayListOf(
                NotesAdapter.Note("Сесть за лабу вовремя", "Без регистрации и смс", Indicator.Yellow.value, true),
                NotesAdapter.Note("Промспать первуб пару", "А то экстрима в жизни не хватает", Indicator.Blue.value, false),
                NotesAdapter.Note("Подумать о будещем", "Положить сотку в карман вертровки", Indicator.Green.value, true),
            )),
            GroupsAdapter.Group("Учёба", arrayListOf(
                NotesAdapter.Note("Сесть за лабу вовремя", "Без регистрации и смс", Indicator.Yellow.value, true),
                NotesAdapter.Note("Промспать первуб пару", "А то экстрима в жизни не хватает", Indicator.Blue.value, false),
                NotesAdapter.Note("Подумать о будещем", "Положить сотку в карман вертровки", Indicator.Green.value, true),
            )),
            GroupsAdapter.Group("Учёба", arrayListOf(
                NotesAdapter.Note("Сесть за лабу вовремя", "Без регистрации и смс", Indicator.Yellow.value, true),
                NotesAdapter.Note("Промспать первуб пару", "А то экстрима в жизни не хватает", Indicator.Blue.value, false),
                NotesAdapter.Note("Подумать о будещем", "Положить сотку в карман вертровки", Indicator.Green.value, true),
            )),
            GroupsAdapter.Group("Учёба", arrayListOf(
                NotesAdapter.Note("Сесть за лабу вовремя", "Без регистрации и смс", Indicator.Yellow.value, true),
                NotesAdapter.Note("Промспать первуб пару", "А то экстрима в жизни не хватает", Indicator.Blue.value, false),
                NotesAdapter.Note("Подумать о будещем", "Положить сотку в карман вертровки", Indicator.Green.value, true),
            )),
            GroupsAdapter.Group("Учёба", arrayListOf(
                NotesAdapter.Note("Сесть за лабу вовремя", "Без регистрации и смс", Indicator.Yellow.value, true),
                NotesAdapter.Note("Промспать первуб пару", "А то экстрима в жизни не хватает", Indicator.Blue.value, false),
                NotesAdapter.Note("Подумать о будещем", "Положить сотку в карман вертровки", Indicator.Green.value, true),
                NotesAdapter.Note("Удали меня", "Ничего страшного не произойдёт", Indicator.Red.value, false),
            ))
        )
    }
}